from __future__ import print_function

template = """\
<%!
import os
import datetime
import math
def pretty_size(size):
    for unit in ['', 'K', 'M', 'G']:
        if size < 1024:
            return '{}{}'.format(size, unit)
        size = int(math.floor(size / 1024 + 0.5))
    return '{}G'.format(size)
%>\
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<html>
 <head>
  <title>Index of ${subdir}</title>
 </head>
 <body>
<h1>Index of ${subdir}</h1>
  <table>
   <tr><th valign="top"><img src="${icondir}/blank.gif" alt="[ICO]"></th><th>Name</th><th>Last modified</th><th>Size</th><th>Description</th></tr>
   <tr><th colspan="5"><hr></th></tr>
<tr><td valign="top"><img src="${icondir}/back.gif" alt="[PARENTDIR]"></td><td><a href="../">Parent Directory</a></td><td>&nbsp;</td><td align="right">-</td><td>&nbsp;</td></tr>

% for entry in entries:
    % if os.path.isdir(entry):
<tr><td valign="top"><img src="${icondir}/folder.gif" alt="[DIR]"></td><td><a href="${os.path.basename(entry)}/">${os.path.basename(entry)}/</a></td><td align="right">${datetime.datetime.fromtimestamp(os.path.getmtime(entry)).strftime('%Y-%m-%d %H:%M')}</td><td align="right">  - </td><td>&nbsp;</td></tr>
    % else:
<tr><td valign="top"><img src="${icondir}/compressed.gif" alt="[ ]"></td><td><a href="${os.path.basename(entry)}">${os.path.basename(entry)}</a></td><td align="right">${datetime.datetime.fromtimestamp(os.path.getmtime(entry)).strftime('%Y-%m-%d %H:%M')}</td><td align="right">${pretty_size(os.path.getsize(entry))}</td><td>&nbsp;</td></tr>
    % endif
% endfor

   <th><td colspan="5"><hr></th></tr>

  </table>
<address>Gitlab Pages at www.mesa3d.org</address>
 </body></html>
"""

import os
from mako.template import Template

def build_index(currdir, icondir, subdir=''):
    entries = [f for f in os.listdir(currdir) if not f.startswith('.')]
    entries = [os.path.join(currdir, f) for f in entries]
    entries.sort()

    html = Template(template).render(subdir=subdir, entries=entries, icondir=icondir)
    with open(os.path.join(currdir, 'index.html'), 'w', encoding='utf-8') as f:
        f.write(html)

    for entry in entries:
        if os.path.isdir(entry):
            build_index(entry, '../' + icondir, subdir + '/' + os.path.basename(entry))

# print(Template(template).render(opcodes=opcodes))
build_index('public', '.icons', '/archive')
# print(os.listdir('./public/'))
